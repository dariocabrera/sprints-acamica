const passport = require("passport")
const LinkedinStrategy = require("passport-linkedin-oauth2").Strategy
const Strategy_name = "linkedin"
require("dotenv").config()

passport.use(Strategy_name, new LinkedinStrategy({
    clientID: process.env.LINKEDIN_CLIENT_ID,
    clientSecret: process.env.LINKEDIN_CLIENT_SECRET,
    callbackURL: process.env.LINKEDIN_CALLBACK,
    //scope: ['r_liteprofile','r_emailaddress']
  },
  function(accessToken, refreshToken, profile, done) {
    // User.findOrCreate({ googleId: profile.id }, function (err, user) {
    //   return done(err, user);
    // });
    return done(null,profile)
  }
));