const express = require('express');
const router = express.Router();
const Strategy_name = 'google';
const passport = require('passport');
const jwt = require('jsonwebtoken');


router.get(`/${Strategy_name}/auth`, passport.authenticate(Strategy_name, {session:false, scope:['profile','email']}));

router.get(`/${Strategy_name}/callback`,
  passport.authenticate(Strategy_name, { session: false, failureRedirect: '/failed', failureMessage: true }),
  function(req, res) {
    console.log(`peticion get /${Strategy_name}/callback`)
    const data = req.user
    console.log("data:")
    console.log(data)
    const token = jwt.sign({ id: data._id }, "mysecretkey", {
      expiresIn: 60 * 60 * 24,
    })
  //const token = "hgjsd8fs6g7s7df67g6sdf43sdg2s3df5sg6s7df7"
    const url_back = process.env.URL_BACK+`/api-docs/?token=${token}`
    res.redirect(301,url_back)
  });

module.exports = router;